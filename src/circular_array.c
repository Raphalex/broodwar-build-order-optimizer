#include "circular_array.h"

Array new_array(){
    return NULL;
}

Array add_array(Array a,Buildable_int_couple i){
    Array new_element= (Array) malloc(sizeof(struct array));
    new_element->v=i;
    if(a==NULL){
        new_element->n=new_element;
        new_element->p=new_element;
        return new_element;
    }
    new_element->n=a->n;
    a->n->p=new_element;
    new_element->p=a;
    a->n=new_element;
    return new_element;
}

Array remove_array(Array a,Buildable_int_couple i){
    if(a->n==a){
        free(a);
        a=NULL;
        return a;
    }
    Array temp=a;
    a->p->n=a->n;
    a->n->p=a->p;
    a=a->p;
    free(temp);
    return a;
}

Iterator init(Array a){
    Iterator new = (Iterator) malloc (sizeof(struct iterator));
    new->beg = a;
    new->cur= a;
    return new;
}

Buildable_int_couple value(Iterator i){
    return i->cur->v;
}

Iterator next(Iterator i){
    i->cur=i->cur->n;
    return i;
}

Iterator previous(Iterator i){
    i->cur=i->cur->p;
    return i;
}

int end(Iterator i){
    return i->cur==i->beg;
}

Iterator jump_to_end(Iterator i){
    i->cur=i->beg;
    return i;
}

int included_in(Array a,Array b){
    Iterator i=init(a);
    Iterator j=init(b);
    int test=0;
    do{
        do{
            i=next(i);
            test+=(value(i)==value(j));
        }while(!end(i));
        j=next(j);
    }while(!end(j) && test);
    return !end(j);
}

int is_in(Array a, Buildable_int_couple c){//Forgot to make this function before the previous one...
    if(a==NULL){
        return 0;
    }
    Iterator i=init(a);
    int test=0;
    do {
        test+=(value(i)->c==c->c? 1:0);
    } while(!end(i));
    return test;
}

Array fuse(Array a){
    Array new=new_array();
    Iterator i=init(a);
    Iterator j;
    do{
        if(is_in(new,value(i))){
            j=init(new);
            do{
                if(value(j)->c==value(i)->c){
                    value(j)->n+=1;
                }
            }while(!end(j));
        }
        else{
            new=add_array(new,new_couple(value(i)->c,1));
        }
    }while(!end(i));
    return new;
}

void print_array(Array a){
    if(a==NULL){
        printf("empty array \n");
        return;
    }
    Array temp=a;
    do{
        printf("%s, ",temp->v->c->name);
        temp=temp->n;
    }while(temp!=a);
    printf("\e[2D");
    printf("\e[K");
    printf(".");
    printf("\n");
}
