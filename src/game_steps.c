#include "game_steps.h"

void resolve_money_and_time(){
    extern int* time;
    extern float* m, * g;
    *time+=1;
    extern float worker_rate_m;
    extern float worker_rate_g;
    extern int* workers_mining_m;
    extern int* workers_mining_g;
    *m+=worker_rate_m*(*workers_mining_m);
    *g+=worker_rate_g*(*workers_mining_g);
}

void resolve_building_terran(){
    extern int* time;
    extern Array *building;
    extern Array *built;
    extern int* workers_mining_m;
    extern int* supply;
    Iterator i;
    i = init(*building);
    do{
        if(value(i)->c->building==NULL){
            if((*time - value(i)->start_time==value(i)->c->time)){
                *built=add_array(*built,new_couple(value(i)->c,*supply)); /*The buildable is added to the *built array with the supply. We can use the supply for *buildings that are producing (we can put a negative value to indicate that it is
                  already producing a unit or an upgrade).*/
              }
              else if(*time -value(i)->start_time==value(i)->c->time + WORKER_RETURN_TIME){ /* The scv can be added back to the mineral line only if it completed its travel time. This value can be found in game_steps.h */
                  int temp=*workers_mining_m + 1;
                  workers_mining_m=&temp;
                  *building = remove_array(*building,(value(i)));
              }
          }
          i=next(i);
    }while(!end(i));
}

void resolve_units_upgrades_terran(){
    extern int* time;
    extern Array *building;
    extern Array *built;
    Iterator i;
    i=init(*building);
    do{
        if((*time -value(i)->start_time==value(i)->c->time)){
            *built=add_array(*built,value(i));
            *building=remove_array(*building,value(i));
            Iterator j;
            j=init(*built);
            do{
                if(value(j)->start_time<0){
                    value(j)->start_time*=-1;
                    j= previous(jump_to_end(j));
                }
                j=next(j);
            }while(!end(j));
        }
    }while(!end(i));
}



void build_unit(Buildable c){
    extern int* time;
    extern float* m, * g;
    extern Array *building;
    extern Array *built;
    int test=0;
    Iterator i;
    if(unlocked(c) && enough_money(c)){
        i=init(*built);
        do{
            if(c->building==value(i)->c && value(i)->start_time>0){
                test=1;
                value(i)->start_time*=-1;
                i=previous(jump_to_end(i));
            }
            i=next(i);
        }while(!end(i));
        if(test){
            Buildable_int_couple c_couple = (Buildable_int_couple) malloc(sizeof(struct buildable_int_couple));
            c_couple->start_time=*time;
            *building = add_array(*building,c_couple);
            *m-=c->m_cost;
            *g-=c->g_cost;
        }
    }
}

void build_terran_building(Buildable c){
    extern int* time;
    extern int* workers_mining_m;
    extern float* m, * g;
    extern Array *building;
    if(unlocked(c) && enough_money(c)){
        Buildable_int_couple c_couple = new_couple(c,*time);
        *building = add_array(*building,c_couple);
        *m-=c->m_cost;
        *g-=c->g_cost;
        workers_mining_m--;
    }
}
