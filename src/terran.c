#include "terran.h"

void load_terran(){
    extern Buildable terran_all[4];
    extern Array * built;
    extern int* supply;
    CC = new_building("Command Center",400,0,75,BUILDABLE_END);
    SCV = new_unit("SCV",50,0,13,1,CC,CC,BUILDABLE_END);
    DEPOT = new_building("Supply Depot",100,0,25,BUILDABLE_END);
    BARRACKS = new_building("Barracks",150,0,50,DEPOT,BUILDABLE_END);

    Buildable_int_couple starting_cc=new_couple(CC,1);
    Buildable_int_couple starting_workers=new_couple(SCV,4);

    *built=add_array((add_array(new_array(),starting_cc)),starting_workers);
    *supply=CC_SUPPLY;
}
