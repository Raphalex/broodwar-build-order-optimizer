#include "buildables.h"
#include "circular_array.h"
#include <stdarg.h>

Buildable new_building(char* name,int m_cost,int g_cost,int time,...){
    Buildable new=(Buildable) malloc(sizeof(struct buildable));
    new->name=name;
    new->m_cost=m_cost;
    new->g_cost=g_cost;
    new->time=time;
    new->supply=0;
    new->building=NULL;
    va_list list;
    va_start(list,time);
    int i=0;
    Buildable dependence=va_arg(list,Buildable);
    Buildable * depends=(Buildable *) malloc (sizeof(Buildable));
    while(dependence!=BUILDABLE_END){ /* *This does through the last arguments and places them in the 'depends' array of the new building */
        depends[i]=dependence;
        dependence=va_arg(list, Buildable);
    }
    va_end(list);
    new->depends=depends;
    return new;
}

Buildable new_unit(char* name,int m_cost,int g_cost,int time,int supply,Buildable building,...){
    Buildable new=(Buildable) malloc(sizeof(struct buildable));
    new->name=name;
    new->m_cost=m_cost;
    new->g_cost=g_cost;
    new->time=time;
    new->building=building;
    new->supply=supply;
    va_list list;
    va_start(list,building);
    int i=0;
    Buildable dependence=NULL;
    Buildable * depends=(Buildable *) malloc (sizeof(Buildable));
    while(dependence!=BUILDABLE_END){ /* *This does through the last arguments and places them in the 'depends' array of the new building */
        dependence=va_arg(list, Buildable);
        depends[i]=dependence;
    }
    va_end(list);
    new->depends=depends;
    return new;
}

Buildable new_upgrade(char* name,int m_cost,int g_cost,int time,Buildable building,...){
    Buildable new=(Buildable) malloc(sizeof(struct buildable));
    new->name=name;
    new->m_cost=m_cost;
    new->g_cost=g_cost;
    new->time=time;
    new->building=building;
    new->supply=0;
    va_list list;
    va_start(list,building);
    int i=0;
    Buildable dependence=NULL;
    Buildable * depends=(Buildable *) malloc (sizeof(Buildable));
    while(dependence!=BUILDABLE_END){ /* *This does through the last arguments and places them in the 'depends' array of the new building */
        dependence=va_arg(list, Buildable);
        depends[i]=dependence;
    }
    va_end(list);
    new->depends=depends;
    return new;

}

int unlocked(Buildable c){
    extern Array *built;
    int j;
    int count=0;
    Iterator i;
    for(j=0;c->depends[j]!=BUILDABLE_END;j++){
        i=init(*built);
        count+=(value(i)->c==c->depends[j]? 1:0);
    }
    return count>=j;
}

int enough_money(Buildable c){
    extern float * m, * g;
    return (*m>=c->m_cost && *g>=c->g_cost);
}

Buildable_int_couple new_couple(Buildable c, int n){
    Buildable_int_couple new = (Buildable_int_couple) malloc(sizeof(struct buildable_int_couple));
    new->c = c;
    new->n = n;
    return new;
}
