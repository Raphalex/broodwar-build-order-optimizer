#include "buildables.h"
#include <stdio.h>
#include "circular_array.h"
#include "game_steps.h"
#include "terran.h"
#include "game_setup.h"

Buildable BUILDABLE_END=NULL;

/* Definition of the end characters */
struct buildable_int_couple buildable_int_couple_end={NULL,0};
Buildable_int_couple BUILDABLE_INT_COUPLE_END=&buildable_int_couple_end;

    /* Initialization time ! */

/* Initializes every variable needed so that the extern variables in other files work. */
float worker_rate_g;
float worker_rate_m;
int mining_g=0;
int mining_m=4;
int* workers_mining_g=&mining_g;
int* workers_mining_m=&mining_m;
int p=0;
int sup=0;
int* supply=&sup;
float minerals=50;
float gas=0;
float* m=&minerals;
float* g=&gas;
int t=0;
int* time=&t;
Array * building;
Array * built;
Buildable terran_all[4];
Array todo_array;



int main(){
    Iterator i;
    Array built_bis=new_array();
    Array building_bis=new_array();
    built=&built_bis;
    building=&building_bis;




    worker_rate_g=SCV_RATE_G;
    worker_rate_m=SCV_RATE_M;
    *supply=CC_SUPPLY;
    load_terran(terran_all,built,supply); // Let's load the terran constants in the terran_all array.

    /* Now let's see what happens each loop... Let set an exemple where we want a barracks as fast as possible.*/
    Buildable_int_couple barracks = new_couple(BARRACKS,1);
    Array request=add_array(new_array(),barracks);
    todo_array = todo(request);

    printf("To build :");
    print_array(request);
    printf("We need to build :");
    print_array(todo_array);

    while(!included_in(request,fuse(*built))){

    i = init(todo_array);
    do{
        if(value(i)->c->building==NULL){/* This differentiates buildings from units and upgrades */
            build_terran_building(value(i)->c);
        }
        else{
            build_unit(value(i)->c);
        }
        i=next(i);
    }while(!end(i));

    if(*building!=NULL){
        resolve_building_terran();
        resolve_units_upgrades_terran();
    }
    resolve_money_and_time();
    }



    return 0;
}
