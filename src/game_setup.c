#include "game_setup.h"

Array todo(Array request){
    Array todo_array=new_array();
    Iterator i=init(request);
    int dependence=0;
    do{
        while(value(i)->c->depends[dependence]!=BUILDABLE_END){
            Buildable_int_couple couple = new_couple(value(i)->c->depends[dependence],1);
            todo_array = add_array(todo_array,couple);
            dependence++;
        }
        todo_array = add_array(todo_array,value(i));
    }while(!end(i));
    return todo_array;
}
