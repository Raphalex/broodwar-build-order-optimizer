# Broodwar Build Order Optimizer

This is a tool to calculate the optimal way to get certain units or buildings in the shortest amount of time.

It will come with options to choose to cut worker production or not and maybe some other options as the project is improved.