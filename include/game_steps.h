#ifndef __GAME_STEPS_H__
#define __GAME_STEPS_H__
#include "buildables.h"
#include "circular_array.h"
#define SCV_RATE_M 1.08
#define SCV_RATE_G 17.1
#define DRONE_RATE_M 67.1
#define DRONE_RATE_G 17.1
#define PROBE_RATE_M 68.1
#define PROBE_RATE_G 17.1
#define CC_SUPPLY 10
#define NEXUS_SUPPLY 9
#define HATCH_SUPPLY 1
#define WORKER_RETURN_TIME 2
/* Below are all the functions used to recalculate minerals, increment the time variable etc... They usually are called each time we go through the loop.
Constants are based on the StarCraft Broodwar Liquipedia. Keep in mind that the values are approximations
since the game is quite random on this aspect. */


void resolve_money_and_time(); /* This function is
used to update the mineral and gas counts aswell as the timer (basically the number of loops)*/
void resolve_building_terran();/* This function is used to see which buildings are finished and to reattribute workers to minerals*/

void resolve_units_upgrades_terran();/*This functions is the same as the previous one but for units and upgrades */


void build_unit(Buildable c);

void build_terran_building(Buildable c);
#endif
