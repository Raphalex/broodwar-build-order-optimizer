#ifndef __CIRCULAR_ARRAY_H__
#define __CIRCULAR_ARRAY_H__
#include "buildables.h"
#include <stdio.h>
typedef struct array{ /*Circular arrays used for the 'building' array */
    Buildable_int_couple v; //Value
    struct array* n;
    struct array *p;
}*Array;

typedef struct iterator{
    Array beg;
    Array cur;
}*Iterator;

Array new_array();

Array add_array(Array a,Buildable_int_couple i);

Array remove_array(Array a,Buildable_int_couple i);

Iterator init(Array a);

Buildable_int_couple value(Iterator i);

Iterator next(Iterator i);

Iterator previous(Iterator i);

int end(Iterator i);

Iterator jump_to_end(Iterator i);

int included_in(Array a, Array b);

int is_in(Array a,Buildable_int_couple c); /* This function actually compares the buildables inside the couples */

Array fuse(Array a); /*This one is a little specific : it takes an array where there are multiple instances of the same buildable,
 and makes a new one where there are only unique couples consisting of the buildable itself, and the number of the same on in the
 original array */


void print_array(Array a); //Used for tests.

#endif
