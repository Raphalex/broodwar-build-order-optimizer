#ifndef __BUILDABLES_H__
#define __BUILDABLES_H__
#define start_time n
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

/* * Buildable type : everything that can be built in the game.*/
typedef struct buildable{
    char* name; //Name
    int m_cost; //Mineral m_cost
    int g_cost; //gaz_cost
    int time; //Building time
    int supply; //Amout of supply the buildable takes (in case it's a unit)
    struct buildable ** depends; //List of what this buildable needs to be available
    struct buildable * building; //For units and upgrades only, it is a pointer on the building that builds the unit of researches the upgrade. This is NULL for buildings, except for Terran add-ons.
}*Buildable;

typedef struct buildable_int_couple{
    Buildable c;
    int n;
}*Buildable_int_couple;

extern Buildable BUILDABLE_END; /* * End character used when going through an array of buildables. Defined in main.c*/
extern Buildable_int_couple BUILDABLE_INT_COUPLE_END; /* * Same thing for the Buildable_int_couple type. */


Buildable new_building(char* name,int m_cost,int g_cost,int time,...); /* * Returns a buildable (a building in this case) using the m_cost, g_cost and time arguments. The last arguments are buildables on which the building depends. The last one must be BUILDABLE_END */

Buildable new_unit(char* name,int m_cost,int g_cost,int time,int supply, Buildable building,...);/* * Returns a buildable (a unit in this case) using the m_cost, g_cost, time, supply and building arguments. The last arguments are buildables on which the building depends. The last one must be BUILDABLE_END */

Buildable new_upgrade(char* name,int m_cost,int g_cost,int time, Buildable building,...);/* * Returns a buildable (an upgrade in this case) using the m_cost, g_cost, time and building arguments. The last arguments are buildables on which the building depends. The last one must be BUILDABLE_END */

int unlocked(Buildable c); /* * Returns 1 if the buildable is unlocked (all things it depends on have been built) 0 if it's stil locked. */

int enough_money(Buildable c); /* *Returns 1 if the player has enough mineral (*m) and gas (*g) to build c */

Buildable_int_couple new_couple(Buildable c, int n);

#endif
